# Advent Of Code
## About
This repository holds all of the code for solving "Advent Of Code" problems as I complete them.

## Compiling
Change directories to the day you want to run, and replace my data with your data, compile with rustc, and
then run! 
