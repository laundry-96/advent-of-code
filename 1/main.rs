use std::collections::HashSet;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

fn main() {

    let data_file = File::open("data.txt").unwrap();
    let data_vec = parse_data(data_file);

    println!("The resulting frequency is {:?}", calculate_resulting_frequency(&data_vec));
    println!("The first recurring frequency is {:?}", calculate_recurring_frequency(&data_vec));
}


fn parse_data(data_file: File) -> Vec<i32> {

    let mut data_vec: Vec<i32> = vec![];
    let data_buffer = BufReader::new(&data_file);

    for line in data_buffer.lines() {
        let data: i32 = line.unwrap().parse().unwrap();
        data_vec.push(data);
    }

    return data_vec;
}

fn calculate_resulting_frequency(data_vec: &Vec<i32>) -> i32 {
    return data_vec.iter().sum()    
}

fn calculate_recurring_frequency(data_vec: &Vec<i32>) -> i32 {

    let mut total = 0;
    let mut resulting_frequencies = HashSet::new();

    loop {
        for data in data_vec {
            total += data;

            if resulting_frequencies.contains(&total) {
                return total;
            }

            else {
                resulting_frequencies.insert(total);
            }
        }
    }
}
